=========
Changelog
=========

Version 1.0.0
===========

- Basic functionalities implemented
- Only HTTP proxying without SSL is supported right now
